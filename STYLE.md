This project has no specific strict code style rules for the Kotlin code. As a general approach, try to make code easy to read and consistently formatted. Some general guidelines and overarching conventions are listed below.

Don't be afraid to break the consistency if it makes for more readable code.

## Rules

There are only two hard and fast rules:

* Indent using four spaces, not tabs.
* Use Line Feed as the line separator (unless the file format requires otherwise).

## Conventions

The general conventions are as follows, in order of importance:

### Braces (usually) get a line to themselves

The rationale is to give some breathing room between e.g. function signatures and the function body. Otherwise it can look quite cramped.

Opening `{` braces should be placed on the next line:
```kotlin
fun myFunction()
{
    // do stuff
}
```

`else`, `catch`, `finally`, etc. should be placed on their own line as well:

```kotlin
if (condition)
{
    // ...
}
else
{
    // ...
}

```

Lambdas are an important exception:

```kotlin
val thisIsPartOfADsl = thisBraceShouldBeOnThisLine {

}
```

Either IntelliJ or Kotlin sometimes have trouble parsing lambdas if the opening brace is on the next line, so this is mostly a matter of avoiding compile-time errors or IDE false positives.

### Align multi-line code if it improves readability

This includes (but is not limited to) function parameters, chained method calls, and chained binary operators.

For example:

```kotlin
val something = functionWithLongParameterNames(first = myFirstParameter,
                                               second = mySecondParameter)
```

looks much nicer than:

```kotlin
val something = functionWithLongParameterNames(first = myFirstParameter,
    second = mySecondParameter)
```

Similarly:

```kotlin
    val something = algeria + botswana + cameroon +
                    djibouti + ethiopia
```

is nice and aligned.

## Bottom line

In general, use your best judgment. The goal is to improve readability, not to enforce arbitrary rules at all costs.
