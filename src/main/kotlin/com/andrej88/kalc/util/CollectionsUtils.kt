package com.andrej88.kalc.util

fun <T> List<T>.findLastWithIndex(predicate: (T) -> Boolean): Pair<T, Int>? {

    for (i in (this.size - 1) downTo 0)
    {
        val e = get(i)
        if (predicate(e))
        {
            return e to i
        }
    }
    return null
}

/**
 * Like [findLastWithIndex], but ensures that the found value is of type [R].
 *
 * If we didn't care about about the index, we could just do:
 *
 * list.filterIsInstance<SomeR>().findLast { ... }
 *
 * However, if we *do* care about the index, just using findLastWithIndex() won't cut it.
 * The index returned using that approach will be the index in the *filtered* list, not the
 * original list (which we care about).
 */
inline fun <T, reified R : T> List<T>.findLastOfTypeWithIndex(predicate: (R) -> Boolean): Pair<R, Int>? {

    for (i in (this.size - 1) downTo 0)
    {
        val e = get(i)
        if (e is R && predicate(e))
        {
            return e to i
        }
    }
    return null
}

fun <T> List<T>.subList(range: IntRange): List<T> = subList(range.start, range.endInclusive + 1)