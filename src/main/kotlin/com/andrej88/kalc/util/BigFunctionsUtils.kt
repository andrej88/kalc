package com.andrej88.kalc.util

import java.math.BigDecimal
import java.math.MathContext


fun BigDecimal.pow(exponent: BigDecimal, scale: Int = 20): BigDecimal =
        BigFunctions.exp(BigFunctions.ln(this, scale).multiply(exponent), scale)
            .round(MathContext(scale - 3))