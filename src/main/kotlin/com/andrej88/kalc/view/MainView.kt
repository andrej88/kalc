package com.andrej88.kalc.view

import com.andrej88.kalc.app.Parser
import javafx.scene.input.KeyCode
import javafx.scene.layout.Priority
import org.fxmisc.flowless.VirtualizedScrollPane
import org.fxmisc.richtext.InlineCssTextArea
import tornadofx.*

class MainView : View("kalc")
{
    override val root = vbox {

        // These don't have DSL-style functions since they're from the RichTextFX library,
        // so we need to create them using constructors directly and then adding them with
        // addChildIfPossible():

        val outputArea = InlineCssTextArea().apply {
            isEditable = false
            isFocusTraversable = false
        }

        val scrollPane = VirtualizedScrollPane(outputArea).apply {
            isFocusTraversable = false
            vgrow = Priority.ALWAYS
        }

        addChildIfPossible(scrollPane)

        val inputField = textfield {

            // TODO: make this be given by whatever creates the view?
            val parser = Parser()

            setOnKeyPressed { keyEvent ->
                if (keyEvent.code == KeyCode.ENTER)
                {

                    val input: String = this@textfield.text

                    // 1. validate input, if invalid then display error and exit function
                    // TODO

                    // 2. evaluate input
                    val result = parser.parse(input)

                    // 3. Add evaluated expression to output
                    outputArea.appendText("$input\n\t")
                    val startBold = outputArea.text.length
                    outputArea.appendText(result)
                    val endBold = outputArea.text.length
                    outputArea.appendText("\n")
                    val startSmallText = outputArea.text.length
                    outputArea.appendText(" ")
                    val endSmallText = outputArea.text.length
                    outputArea.appendText("\n")
                    outputArea.setStyle(startBold, endBold, "-fx-font-weight: bold;")

                    // Adds a bit of space between Input/Output pairs:
                    outputArea.setStyle(startSmallText, endSmallText, "-fx-font-size: 5;")

                    // Scroll to the bottom, and also to the left, just in
                    // case we were scrolled over to the right.

                    // For some reason, this works, even though scrollPane
                    // should be handling the scrolling...?
                    // Is ScrollPane just a visual wrapper around something
                    // that actually has scrolling "behavior"?
                    outputArea.scrollYBy(Double.MAX_VALUE)
                    outputArea.scrollXToPixel(0.0)

                    // 4. Clear input
                    this@textfield.clear()
                }
            }
        }
    }
}