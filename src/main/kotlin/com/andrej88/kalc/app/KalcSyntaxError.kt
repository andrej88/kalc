package com.andrej88.kalc.app

/**
 * The subclasses of this sealed class represent various problems that can arise when
 * parsing an input string.
 */
sealed class KalcSyntaxError(message: String? = null) : Exception(message)
{
    /**
     * If we encounter "()"
     */
    class EmptyParentheses : KalcSyntaxError()

    /**
     * If we have an open parenthesis without a matching closing parenthesis.
     */
    class UnclosedParentheses : KalcSyntaxError()

    /**
     * If we encounter closing parenthesis without having encountered an opening parenthesis.
     */
    class UnopenedParentheses : KalcSyntaxError()

    /**
     * If we see a character we have no idea what to do with.
     */
    class UnexpectedSymbol(symbol: Char, position: Int) :
            KalcSyntaxError("Unexpected symbol \"$symbol\" at position $position")

    /**
     * If we encounter a number with two decimal points
     */
    class TooManyDecimalPoints : KalcSyntaxError()

    /**
     * The expression ended in a binary operator
     */
    class TrailingBinaryOperator : KalcSyntaxError()

    /**
     * When we have too many binary operators in a row
     */
    class TooManyOperators : KalcSyntaxError()

    /**
     * When we encounter a binary operator in an unexpected position.
     */
    class UnexpectedBinaryOperator : KalcSyntaxError()

    /**
     * When encountering two numbers separate by a space.
     */
    class MissingBinaryOperator : KalcSyntaxError()
}