package com.andrej88.kalc.app

import com.andrej88.kalc.app.math.BinaryOperator
import java.math.BigDecimal

/**
 * The subclasses of this sealed class represent the semantic tokens that make up
 * an input. These are used when parsing an input, but before it's formed into a
 * syntax tree for evaluation.
 */
sealed class Token
{
    /**
     * Represents a [BinaryOperator]
     */
    class BinOperator(val operator: BinaryOperator) : Token()

    sealed class Value : Token()
    {
        /**
         * Represents a number.
         */
        class Numeric(val number: BigDecimal) : Value()

        /**
         * Represents a parenthetical group, which itself will need to be parsed.
         */
        class Group(val string: String) : Value()

    }

    enum class Type
    {
        VALUE, BIN_OPERATOR
    }
}