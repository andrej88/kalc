package com.andrej88.kalc.app

import com.andrej88.kalc.view.MainView
import com.andrej88.kalc.view.Styles
import javafx.scene.image.Image
import javafx.stage.Stage
import tornadofx.App
import tornadofx.addStageIcon

class MyApp : App(MainView::class, Styles::class)
{
    init
    {
        addStageIcon(Image("icon 16.png"), scope)
        addStageIcon(Image("icon 24.png"), scope)
        addStageIcon(Image("icon 32.png"), scope)
        addStageIcon(Image("icon 48.png"), scope)
    }

    override fun start(stage: Stage)
    {
        super.start(stage)

        stage.minWidth = WINDOW_HEIGHT
        stage.minHeight = WINDOW_WIDTH

        stage.centerOnScreen()
    }

    companion object
    {
        const val WINDOW_HEIGHT = 400.0
        const val WINDOW_WIDTH = 300.0
    }
}