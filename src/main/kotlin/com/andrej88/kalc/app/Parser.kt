package com.andrej88.kalc.app

import com.andrej88.kalc.app.math.BinaryOperator
import com.andrej88.kalc.util.findLastOfTypeWithIndex
import com.andrej88.kalc.util.subList
import java.math.BigDecimal

class Parser
{
    fun parse(string: String): String
    {
        var result = parseToBigDecimal(string).toPlainString()

        if ('.' in result)
        {
            result = result.trimEnd('0').trimEnd('.')
        }

        return result

        // return parseBigDecimal(string).toPlainString().trim('0').trim('.')
    }

    private fun parseToBigDecimal(string: String): BigDecimal
    {
        val trimmed = string.trim()
        // trim start and end parentheses to avoid going in an infinite loop
        val stringForProcessing = if (trimmed.startsWith('(') && trimmed.endsWith(')'))
        {
            if (trimmed == "()")
            {
                throw KalcSyntaxError.EmptyParentheses()
            }

            trimmed.substring(1..string.length - 2)
        }
        else
        {
            trimmed
        }

        // String -> List<Token> (dirty) -> List<Token> (clean) -> BigDecimal

        return evaluate(validate(tokenize(stringForProcessing)))
    }

    private fun tokenize(string: String): List<Token>
    {
        // We'll add tokens to this list and return it at the end.
        val tokens = mutableListOf<Token>()


        // And now for some good old fashioned while-loop style iteration
        // #whilestyle

        var i = 0
        stringLoop@while (i < string.length)
        {
            var c = string[i]

            when (c)
            {
                '(' ->
                {
                    // Case 1: Identify parenthetical groups

                    val openParenIndex = i

                    // fast forward until we reach ')'. We don't care about what's in the parentheses (yet)
                    var nestedParenLevel = 0
                    i++
                    parenFF@ while (i < string.length)
                    {
                        val innerC = string[i]

                        if (innerC == '(')
                        {
                            nestedParenLevel++
                        }
                        else if (innerC == ')')
                        {
                            if (nestedParenLevel > 0)
                            {
                                nestedParenLevel--
                            }
                            else
                            {
                                break@parenFF
                            }
                        }
                        i++
                    }

                    if (i >= string.length)
                    {
                        // We reached the end of the string without having encountered a ')'
                        throw KalcSyntaxError.UnclosedParentheses()
                    }

                    c = string[i]

                    if (c != ')')
                    {
                        throw ImpossibleError()
                    }

                    // Let's add this sequence we just ran through as a parenthetical token
                    tokens.add(Token.Value.Group(string.substring(openParenIndex..i)))
                }


                in BinaryOperator.allSymbols ->
                {
                    // Case 1: Create binary operator tokens

                    // Let's figure out which operator we matched.
                    // If we're in this block, then we must have matched a valid operator symbol,
                    // and the exception should never be thrown.
                    val operator = BinaryOperator.getOperator(c) ?: throw ImpossibleError()
                    tokens.add(Token.BinOperator(operator))
                }

                in numberSymbols ->
                {
                    // Case 3: Create numerical tokens

                    val numberStartIndex = i

                    do
                    {
                        i++
                    }
                    while (i < string.length && string[i] in numberSymbols)

                    // We overshot a bit, back up
                    i--

                    val numberString = string.substring(numberStartIndex..i)

                    // Now is it *really* number?
                    var numDecimalPoints = 0
                    for (d in numberString)
                    {
                        if (d == '.')
                        {
                            if (numDecimalPoints >= 1)
                            {
                                throw KalcSyntaxError.TooManyDecimalPoints()
                            }
                            numDecimalPoints++
                        }
                    }

                    tokens.add(Token.Value.Numeric(numberString.toBigDecimal()))
                }

                // Following are unexpected symbols:

                ')' ->
                {
                    // uhh we never saw an open parenthesis...
                    throw KalcSyntaxError.UnopenedParentheses()
                }

                ' ' ->
                {
                    // It's just whitespace - fast forward and proceed as normal.
                    // If the whitespace is weirdly located, we'll deal with it the
                    // unexpected tokens in validate().

                    do
                    {
                        i++
                    }
                    while (i < string.length && string[i] == ' ')

                    // We're at the right position, just continue the loop.
                    continue@stringLoop
                }

                else ->
                {
                    // Who knows what we encountered?
                    throw KalcSyntaxError.UnexpectedSymbol(c, i)
                }
            }

            i++
        }

        return tokens
    }

    /**
     * Validates the tokens to ensure there are no syntax errors etc.
     *
     * Cleans the list to try and make sense of potentially problematic tokens (see below).
     *
     * The returned list is a new object.
     *
     * List should start and end with a group or a numerical type.
     * It should alternate between operators and group/numerical types.
     * If two consecutive binary operators are found, and the second is a -,
     * then remove the - and treat the following number as negative.
     */
    private fun validate(tokens: List<Token>): List<Token>
    {
        val cleaned = mutableListOf<Token>()

        var expectedTokenType = Token.Type.VALUE

        var i = 0
        while (i < tokens.size)
        {
            val token = tokens[i]

            // Check if the token we're currently reading is of the type we expected
            val expected: Boolean = when (token)
            {
                is Token.Value -> expectedTokenType == Token.Type.VALUE
                is Token.BinOperator -> expectedTokenType == Token.Type.BIN_OPERATOR
            }

            if (expected)
            {
                // Everything's going swimmingly
                cleaned.add(token)

                expectedTokenType = when (token)
                {
                    is Token.Value -> Token.Type.BIN_OPERATOR
                    is Token.BinOperator -> Token.Type.VALUE
                }

                i++
                continue
            }

            // But if we read something unexpected...

            when (token)
            {
                is Token.BinOperator ->
                {

                    if (token.operator != BinaryOperator.MINUS)
                    {
                        // We can deal with an unexpected "-", but that's it.
                        throw KalcSyntaxError.UnexpectedBinaryOperator()
                    }

                    // See if a number (or parentheses) follows, and if so, make it negative
                    i++

                    if (i >= tokens.size)
                    {
                        throw KalcSyntaxError.TrailingBinaryOperator()
                    }

                    val newToken = tokens[i] as? Token.Value ?:
                                   throw KalcSyntaxError.TooManyOperators()

                    // It's a "-" preceding a value, so make the value negative
                    when (newToken)
                    {
                        is Token.Value.Numeric ->
                        {
                            cleaned.add(Token.Value.Numeric(newToken.number * (-1).toBigDecimal()))
                        }

                        is Token.Value.Group ->
                        {
                            cleaned.add(Token.Value.Group("(-1 * ${newToken.string})"))
                        }
                    }

                    // We shifted over to a value token, so we're expecting an operator now
                    expectedTokenType = Token.Type.BIN_OPERATOR
                }

                is Token.Value ->
                {
                    if (cleaned.last() is Token.Value.Numeric && token is Token.Value.Numeric)
                    {
                        // We're ok with "(3)(4)", "3(4)", and "(3)4", but not "3 4"
                        throw KalcSyntaxError.MissingBinaryOperator()
                    }

                    // Implicit multiplication
                    cleaned.add(Token.BinOperator(BinaryOperator.MULTIPLY))
                    cleaned.add(token)

                    // expectedTokenType is already BinaryOperatorToken
                }
            }

            i++
        }

        return cleaned
    }

    private fun evaluate(tokens: List<Token>): BigDecimal
    {
        if (tokens.size == 1)
        {
            // If we ever throw this error, it indicates a shortcoming of the validate() function.
            // It means we have a single non-value token that somehow wasn't caught before.
            val token = tokens[0] as? Token.Value ?: throw ImpossibleError()

            return when (token)
            {
                is Token.Value.Numeric -> token.number
                is Token.Value.Group -> parseToBigDecimal(token.string)
            }
        }

        var operators = BinaryOperator.order
        var pivot: Pair<Token.BinOperator, Int>?

        do
        {
            pivot = tokens.findLastOfTypeWithIndex { (it.operator in operators.last()) }
            operators = operators.dropLast(1)
        }
        while (pivot == null && operators.isNotEmpty())

        if (pivot == null)
        {
            // If we're here, it means the list of tokens doesn't have an operator.
            // This should never happen, since the list was cleaned in validate()
            throw ImpossibleError()
        }

        return pivot.first.operator.evaluate(evaluate(tokens.subList(0 until pivot.second)),
                                             evaluate(tokens.subList((pivot.second + 1) until (tokens.size))))
    }

    companion object
    {
        val numberSymbols = listOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.')
    }
}