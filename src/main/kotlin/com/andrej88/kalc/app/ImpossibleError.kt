package com.andrej88.kalc.app

/**
 * This should only be thrown if we reach a situation which, logically, cannot happen.
 * In other words, this should only be used in parts of the code that are unreachable
 * without the compiler realizing it. If this ever gets thrown, it means that part of
 * the code is *not*, in fact, unreachable, and we should review it to get a better
 * idea of what's going on.
 */
class ImpossibleError : Exception()