package com.andrej88.kalc.app.math

interface Operator
{
    val symbols: List<Char>
}