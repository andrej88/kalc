package com.andrej88.kalc.app.math

import com.andrej88.kalc.util.pow
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

/**
 * An enum class representing all possible binary operators.
 * 
 * @property symbols All symbols that may be interpreted as this operator. **All symbols must be unique across all operators**.
 * @property evaluate how to evaluate this operator given its two operands.
 */
enum class BinaryOperator(override val symbols: List<Char>,
                          val evaluate: (BigDecimal, BigDecimal) -> BigDecimal) : Operator
{
    PLUS('+', { l, r -> l.add(r, MC) }),
    MINUS('-', { l, r -> l.subtract(r, MC) }),
    MULTIPLY(listOf('*', '×'), { l, r -> l.multiply(r, MC) }),
    DIVIDE(listOf('/', '÷'), { l, r -> l.divide(r, MC) }),
    POWER('^', { l, r -> l.pow(r) })
    ;

    constructor(symbol: Char, evaluator: (BigDecimal, BigDecimal) -> BigDecimal) :
            this(listOf(symbol), evaluator)

    companion object
    {
        // This needs the lazy delegate because there's an initialization loop
        // that would have to be resolved if it were possible to access enum
        // values from the companion object and vice versa. Actually, it might
        // be unnecessary here, can't remember, but it's safer this way.
        val allSymbols by lazy { values().flatMap { it.symbols } }
        
        // Order of operations
        val order: List<List<BinaryOperator>> = listOf(listOf(POWER),
                                                       listOf(MULTIPLY, DIVIDE),
                                                       listOf(PLUS, MINUS))

        fun getOperator(symbol: Char) = BinaryOperator.values().find { symbol in it.symbols }

        val MC = MathContext(20, RoundingMode.HALF_UP)
    }
}