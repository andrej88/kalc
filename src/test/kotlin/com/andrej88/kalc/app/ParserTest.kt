package com.andrej88.kalc.app

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

object ParserTest :
        Spek({

                 describe("a parser") {

                     val parser = Parser()

                     describe("parsing numbers on their own") {

                         it("should evaluate a number as itself") {
                             assertEquals("123", parser.parse("123"))
                         }

                         it("should understand decimal points") {
                             assertEquals("12.3", parser.parse("12.3"))
                         }

                         it("should accept leading zeroes") {
                             assertEquals("123", parser.parse("00123"))
                         }

                         it("should understand a number that starts with a decimal point") {
                             assertEquals("0.123", parser.parse(".123"))
                         }

                         it("should error if it sees multiple decimal points") {
                             assertFailsWith(KalcSyntaxError.TooManyDecimalPoints::class) {
                                 parser.parse("1.2.3")
                             }
                         }

                         // TODO: What to do if the number ends with a decimal point? "."

                     }

                     describe("arithmetic sanity checks") {

                         it("should add two numbers successfully") {
                             assertEquals("7", parser.parse("3 + 4"))
                         }

                         it("should subtract two numbers successfully") {
                             assertEquals("3", parser.parse("5 - 2"))
                         }

                         it("should multiply two numbers successfully") {
                             assertEquals("18", parser.parse("3 * 6"))
                         }

                         it("should divide two numbers successfully") {
                             assertEquals("3", parser.parse("6 / 2"))
                         }
                     }

                     describe("more advanced binary operations") {

                         it("should perform exponentiation successfully") {
                             assertEquals("256", parser.parse("2 ^ 8"))
                         }

                         it("should return the expected value when dividing yields a non-integer") {
                             assertEquals("2.5", parser.parse("5 / 2"))
                         }

                         describe("five basic operators on non-integers") {

                             it("should add successfully") {
                                 assertEquals("2.15", parser.parse("1.65 + 0.5"))
                             }

                             it("should subtract successfully") {
                                 assertEquals("1.25", parser.parse("1.75 - 0.5"))
                             }

                             it("should multiply successfully") {
                                 assertEquals("0.75", parser.parse("1.5 * 0.5"))
                             }

                             it("should divide successfully") {
                                 assertEquals("3.2", parser.parse("1.6 / 0.5"))
                             }

                             it("should exponentiate successfully") {
                                 assertEquals("0.5", parser.parse("0.25 ^ 0.5"))
                             }
                         }

                     }

                     describe("syntax quirks") {

                         it("should not care about whitespace around binary operators") {
                             assertEquals("6", parser.parse("2   * 3"))
                             assertEquals("6", parser.parse("2*3"))
                             assertEquals("6", parser.parse("2* 3"))
                         }

                     }

                     describe("unusual operator symbols") {

                         it("× should be the same as *") {
                             assertEquals(parser.parse("5 * 4"), parser.parse("5 × 4"))
                         }

                         it("÷ should be the same as /") {
                             assertEquals(parser.parse("5 / 4"), parser.parse("5 ÷ 4"))
                         }

                     }

                     describe("chained binary operations") {

                         it("should correctly evaluate multiple binary operators") {
                             assertEquals("8", parser.parse("2 + 4 + 2"))
                         }

                         it("should correctly apply the order of operations") {
                             assertEquals("246", parser.parse("4 / 2 * 7 + 3 ^ 5 - 11"))
                         }

                     }

                     describe("parenthesis grouping") {

                         it("should respect parentheses") {
                             assertEquals("25", parser.parse("(2 + 3) * 5"))
                         }

                         describe("error on invalid parenthesis contents") {

                             it("should error on empty parentheses") {
                                 assertFailsWith(KalcSyntaxError.EmptyParentheses::class) {
                                     parser.parse("3 + ()")
                                 }
                             }

                             it("should error on an invalid expression in parentheses") {
                                 assertFailsWith(KalcSyntaxError::class) {
                                     parser.parse("3 + (4 +* 2)")
                                 }
                             }

                         }

                         describe("unbalanced parentheses") {

                             it("should fail if missing an open parenthesis") {
                                 assertFailsWith(KalcSyntaxError::class) {
                                     parser.parse("3 * 5 + 2)")
                                 }
                             }

                             it("should fail if missing a close parenthesis") {
                                 assertFailsWith(KalcSyntaxError.UnclosedParentheses::class) {
                                     parser.parse("3 * (5 + 2")
                                 }
                             }
                         }

                         describe("nested parentheses") {

                             it("should survive redundant nested parentheses") {
                                 assertEquals("21", parser.parse("3 * ((5 + 2))"))
                             }

                             it ("should handle nested parenthetical groups normally") {
                                 assertEquals("83", parser.parse("2 + (3 ^ (2 * 2))"))
                             }

                         }
                     }

                 }
             })