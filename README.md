[![Apache License](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0)

![Flosh Logo](assets/logo 48.png)

Kalc is a cross-platform desktop calculator that aims to be easy to use, flexible, and powerful.
Some frequently-encountered drawbacks to existing desktop calculators include:

- **Visual GUI buttons** - why keep this skeuomorph? Typing using a mouse is painfully slow, why not let us use a keyboard like every other text input we encounter? The Pre-Windows 10 calculator loves its GUI buttons so much, it hampers the user experience.
- **Overly simplistic functionality** - with modern computers we shouldn't shy away from more complex operations such as arbitrary-base logarithms.
- **Inflexibility** - Most desktop calculators don't let you define your own functions, don't have a nice consistent syntax for calling built-in functions, etc.

Kalc is licensed under the Apache License, Version 2.0. See LICENSE for details.


### Development

Kalc uses Gradle for its build system. To set up Flosh in an IDE, import it as a Gradle project. To run the program, execute Gradle's `run` task.

Kalc is based on TornadoFX, a JavaFX framework for Kotlin. If you use Intellij IDEA, the TornadoFX plugin might come in useful.

Kalc uses the Spek framework for testing. Tests can be run using Gradle's `test` task, but using IntelliJ with the Spek plugin is much more convenient for development.


